package trycatch;

public class Main {

    public static void main(String[] args) {


         double valorVeiculo = 20000.00;
         double entrada = 10000.00;
         int numeroParcelas = 30;


        try {
            Finaciamento f = new Finaciamento(valorVeiculo, entrada, numeroParcelas);
            System.out.println(f.prestacao());
        }
        catch (ParcelaNaoAprovadaException ex ){
            System.out.println(ex.getMessage());
        }

        catch (SemEntradaException ex ){
            System.out.println(ex.getMessage());
        }
        finally {
            System.out.println("Procedimento finalizado");
        }


    }

}
